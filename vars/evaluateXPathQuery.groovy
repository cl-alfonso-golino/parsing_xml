import javax.xml.xpath.*
import javax.xml.parsers.DocumentBuilderFactory

/** Evaluate a XPath query over a String in xml format.
 * Returns a list with all the elements found by the query.
 * References:
 *   - https://web.archive.org/web/20150927053258/http://groovy.jmiguel.eu/groovy.codehaus.org/Reading+XML+with+Groovy+and+XPath.html
 *   - https://stackoverflow.com/a/2269464
 */
def call(String xml, String xPathQuery) {
    def builder = DocumentBuilderFactory.newInstance().newDocumentBuilder()
    def doc = builder.parse(new ByteArrayInputStream(xml.bytes))
    def expr = XPathFactory.newInstance().newXPath().compile(xPathQuery)
    def nodes = expr.evaluate(doc, XPathConstants.NODESET)
    return nodes.collect() { node -> node.textContent }
}